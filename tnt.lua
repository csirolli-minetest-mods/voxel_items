local S = default.get_translator
minetest.register_node(":tnt:tnt_stick", {
    description = S("TNT Stick"),
    tiles = { "tnt_tnt_stick.png" },
    inventory_image = "tnt_tnt_stick.png",
    wield_image = "tnt_tnt_stick.png",
    drawtype = 'mesh',
    paramtype = 'light',
    paramtype2 = 'none',
    selection_box = {
        type = 'fixed',
        fixed = { -8 / 16, -8 / 16, -8 / 16, 8 / 16, -7 / 16, 8 / 16 }
    },
    mesh = 'extrusion_mesh_16.obj',
    use_texture_alpha = 'clip',
    walkable = false,
    is_ground_content = false,
    groups = {
        flammable = 5,
        falling_node = 1,
        oddly_breakable_by_hand = 3,
        snappy = 3,
        choppy = 3,
        attached_node = 1,
        dig_immediate = 3
    },
})

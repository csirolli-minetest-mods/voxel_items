local S = minetest.get_translator("vessels")
if minetest.registered_craftitems["vessels:glass_fragments"] ~= nil then
    minetest.register_node(":vessels:glass_fragments", {
        description = S("Glass Fragments"),
        tiles = {"vessels_glass_fragments.png"},
        inventory_image = "vessels_glass_fragments.png",
        wield_image = "vessels_glass_fragments.png",
        drawtype = "plantlike",
        paramtype = "light",
        is_ground_content = false,
        walkable = false,
        selection_box = {
            type = "fixed",
            fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
        },
        groups = {vessel = 1, dig_immediate = 3, attached_node = 1},
        sounds = default.node_sound_defaults(),
    })
end
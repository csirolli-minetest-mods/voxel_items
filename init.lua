local path = minetest.get_modpath('voxel_items')
if minetest.global_exists('default') then
    dofile(path .. '/default.lua')
    dofile(path .. '/vessels.lua')
end
if minetest.global_exists('tnt') then
    dofile(path .. '/tnt.lua')
end